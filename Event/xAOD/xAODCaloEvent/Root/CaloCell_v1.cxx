#include "xAODCaloEvent/versions/CaloCell_v1.h"


xAOD::CaloCell_v1::CaloCell_v1() {}


int xAOD::CaloCell_v1::hash() const
{
  static const Accessor<int> acc ("hash");
  return acc(*this);
}

void xAOD::CaloCell_v1::setHash (int hash)
{
  static const Accessor<int> acc ("hash");
  acc(*this) = hash;
}


float xAOD::CaloCell_v1::e() const
{
  static const Accessor<float> acc ("e_f");
  return acc(*this);
}


void xAOD::CaloCell_v1::setE (float e)
{
  static const Accessor<float> acc ("e_f");
  acc(*this) = e;
}


float xAOD::CaloCell_v1::eta() const
{
  static const Accessor<float> acc ("eta_f");
  return acc(*this);
}


void xAOD::CaloCell_v1::setEta (float eta)
{
  static const Accessor<float> acc ("eta_f");
  acc(*this) = eta;
}


float xAOD::CaloCell_v1::phi() const
{
  static const Accessor<float> acc ("phi_f");
  return acc(*this);
}


void xAOD::CaloCell_v1::setPhi (float eta)
{
  static const Accessor<float> acc ("phi_f");
  acc(*this) = eta;
}
