#include "xAODCaloEvent/versions/CaloCellAuxContainer_v1.h"


namespace xAOD {


CaloCellAuxContainer_v1::CaloCellAuxContainer_v1()
{
  AUX_VARIABLE( hash );
  AUX_VARIABLE( e_f );
  AUX_VARIABLE( eta_f );
  AUX_VARIABLE( phi_f );
}


} // namespace xAOD
