// -*- c++ -*-

#ifndef XAODCALOEVENT_CALOCELL_H
#define XAODCALOEVENT_CALOCELL_H


#include "xAODCaloEvent/versions/CaloCell_v1.h"


namespace xAOD {
  typedef CaloCell_v1 CaloCell;
}


#include "xAODCore/CLASS_DEF.h"
CLASS_DEF (xAOD::CaloCell, 210428721, 1)


#endif
