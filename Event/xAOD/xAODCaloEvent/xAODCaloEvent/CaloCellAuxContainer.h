// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration.
 */
// $Id$
/**
 * @file xAODCaloEvent/CaloCellAuxContainer.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2018
 * @brief 
 */


#ifndef XAODCALOEVENT_CALOCELLAUXCONTAINER_H
#define XAODCALOEVENT_CALOCELLAUXCONTAINER_H


#include "xAODCaloEvent/versions/CaloCellAuxContainer_v1.h"


namespace xAOD {
   typedef CaloCellAuxContainer_v1 CaloCellAuxContainer;
}

#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::CaloCellAuxContainer, 1318932790, 1 )


#endif // not XAODCALOEVENT_CALOCELLAUXCONTAINER_H
