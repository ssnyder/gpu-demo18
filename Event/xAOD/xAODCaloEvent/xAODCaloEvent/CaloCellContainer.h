// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration.
 */
// $Id$
/**
 * @file xAODCaloEvent/CaloCellContainer.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2018
 * @brief 
 */


#ifndef XAODCALOEVENT_CALOCELLCONTAINER_H
#define XAODCALOEVENT_CALOCELLCONTAINER_H

#include "xAODCaloEvent/versions/CaloCellContainer_v1.h"


namespace xAOD {
   /// Define the latest version of the calorimeter cluster container
   typedef CaloCellContainer_v1 CaloCellContainer;
}

// Set up a CLID for the container:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::CaloCellContainer, 1316377543, 1 )


#endif // not XAODCALOEVENT_CALOCELLCONTAINER_H
