// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration.
 */
// $Id$
/**
 * @file xAODCaloEvent/CaloCell_v1.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2018
 * @brief 
 */


#ifndef XAODCALOEVENT_CALOCELL_V1_H
#define XAODCALOEVENT_CALOCELL_V1_H


#include "AthContainers/AuxElement.h"


namespace xAOD {


class CaloCell_v1
  : public SG::AuxElement
{
public:
  CaloCell_v1();
  int hash() const;
  float e() const;
  float eta() const;
  float phi() const;

  void setHash (int hash);
  void setE (float e);
  void setEta (float eta);
  void setPhi (float phi);
};


} // namespace xAOD


#endif // not XAODCALOEVENT_CALOCELL_V1_H
