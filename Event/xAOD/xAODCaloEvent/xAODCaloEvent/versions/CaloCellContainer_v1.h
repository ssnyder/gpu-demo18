// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration.
 */
// $Id$
/**
 * @file xAODCaloEvent/CaloCellContainer_v1.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2018
 * @brief 
 */


#ifndef XAODCALOEVENT_CALOCELLCONTAINER_V1_H
#define XAODCALOEVENT_CALOCELLCONTAINER_V1_H


#include "AthContainers/DataVector.h"
#include "xAODCaloEvent/versions/CaloCell_v1.h"


namespace xAOD {
   typedef DataVector< CaloCell_v1 > CaloCellContainer_v1;
}


#endif // not XAODCALOEVENT_CALOCELLCONTAINER_V1_H
