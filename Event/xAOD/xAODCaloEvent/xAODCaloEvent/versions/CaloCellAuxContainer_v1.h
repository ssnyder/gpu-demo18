// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration.
 */
// $Id$
/**
 * @file xAODCaloEvent/CaloCellAuxContainer_v1.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2018
 * @brief 
 */


#ifndef XAODCALOEVENT_CALOCELLAUXCONTAINER_V1_H
#define XAODCALOEVENT_CALOCELLAUXCONTAINER_V1_H


#include "xAODCore/AuxContainerBase.h"


namespace xAOD {


class CaloCellAuxContainer_v1
  : public AuxContainerBase
{
public:
  CaloCellAuxContainer_v1();

  std::vector<int> hash;
  std::vector<float> e_f;
  std::vector<float> eta_f;
  std::vector<float> phi_f;
};


} // namespace xAOD


#include "xAODCore/BaseInfo.h"
SG_BASE( xAOD::CaloCellAuxContainer_v1, xAOD::AuxContainerBase );


#endif // not XAODCALOEVENT_CALOCELLAUXCONTAINER_V1_H
