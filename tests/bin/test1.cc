#include <starpu.h>
#include <iostream>


void scal_cuda_func (void* buffers[], void* args);
void scal_opencl_func (void* buffers[], void* args);
void scal_opencl_init();


void cpu_func (void *buffers[], void* cl_arg)
{
  float factor = *reinterpret_cast<float*> (cl_arg);
  unsigned n = STARPU_VECTOR_GET_NX (buffers[0]);
  float* val = reinterpret_cast<float*> (STARPU_VECTOR_GET_PTR (buffers[0]));
  for (unsigned i=0; i < n; i++) {
    val[i] *= factor;
  }
}


int main (int /*argc*/, char** /*argv*/)
{
  int ret = starpu_init (NULL);
  STARPU_CHECK_RETURN_VALUE (ret, "starpu_init");

  scal_opencl_init();

  static const int NX = 2048;
  float v[NX] = {1, 0};
  std::cout << "before ... v[0] = " << v[0] << "\n";

  starpu_data_handle_t vector_handle;
  starpu_vector_data_register (&vector_handle,
                               STARPU_MAIN_RAM,
                               reinterpret_cast<uintptr_t> (v),
                               NX,
                               sizeof (v[0]));

  starpu_codelet cl;
  starpu_codelet_init (&cl);
  cl.cpu_funcs[0] = cpu_func;
  cl.cpu_funcs_name[0] = "cpu_func";
  cl.cuda_funcs[0] = scal_cuda_func;
  cl.opencl_funcs[0] = scal_opencl_func;
  cl.nbuffers = 1;
  cl.modes[0] = STARPU_RW;

  float factor = 2.7;

  starpu_task* task = starpu_task_create();
  task->cl = &cl;
  task->handles[0] = vector_handle;
  task->cl_arg = &factor;
  task->cl_arg_size = sizeof (factor);

  ret = starpu_task_submit (task);
  STARPU_CHECK_RETURN_VALUE (ret, "starpu_task_submit");

  starpu_task_wait_for_all();

  starpu_data_unregister (vector_handle);

  starpu_shutdown();

  std::cout << "after ... v[0] = " << v[0] << "\n";
  return 0;
}
