#include "xAODCaloEvent/CaloCellContainer.h"
#include "xAODCaloEvent/CaloCell.h"
#include "xAODCaloEvent/CaloCellAuxContainer.h"
#include "AthContainers/AuxTypeRegistry.h"
#include "tests/DataVectorAccessor.h"
#include "tests/CaloCellAccessor.h"
#include "tests/DataVectorStarPUInterface.h"
#include "tests/AthStarPU.h"
#include <vector>


void xaodtest_cuda_func (void* buffers[], void* /*args*/);
void xaodtest_cuda_copy (void* buffers[], void* /*args*/);


//************************************************************************


void cpu_func (void *buffers[], void* /*cl_arg*/)
{
  const CaloCellAccessor& acc = dv_accessor<CaloCellAccessor> (buffers[0]);
  unsigned int nv = STARPU_VECTOR_GET_NX (buffers[1]);
  STARPU_ASSERT_MSG (nv == acc.size(), "Size mismatch.");
  float* val = reinterpret_cast<float*> (STARPU_VECTOR_GET_PTR (buffers[1]));
  for (unsigned int i = 0; i < nv; i++) {
      val[i] = acc.e(i) + 2*acc.phi(i);
  }
}


void cpu_func2 (void *buffers[], void* /*cl_arg*/)
{
  const CaloCellAccessor& acc1 = dv_accessor<CaloCellAccessor> (buffers[0]);
        CaloCellAccessor& acc2 = dv_accessor<CaloCellAccessor> (buffers[1]);
  const size_t sz = acc1.size();
  STARPU_ASSERT_MSG (sz == acc2.size(), "Size mismatch.");
  for (unsigned int i = 0; i < sz; i++) {
    acc2.e(i) = acc1.e(i) * 1.5;
    acc2.eta(i) = acc1.eta(i);
    acc2.phi(i) = acc1.phi(i);
    acc2.hash(i) = acc1.hash(i);
  }
}


class MyTask
{
public:
  MyTask();
  void submit (const AthStarPU::Handle<CaloCellAccessor>& h1,
               const starpu_data_handle_t& h2);
  starpu_task* task (const AthStarPU::Handle<CaloCellAccessor>& h1,
                     const starpu_data_handle_t& h2);


private:
  starpu_codelet m_cl;
};


MyTask::MyTask()
{
  starpu_codelet_init (&m_cl);
  m_cl.cpu_funcs[0] = cpu_func;
  m_cl.cuda_funcs[0] = xaodtest_cuda_func;
  m_cl.nbuffers = 2;
  m_cl.modes[0] = STARPU_R;
  m_cl.modes[1] = STARPU_W;
}


void MyTask::submit (const AthStarPU::Handle<CaloCellAccessor>& h1,
                     const starpu_data_handle_t& h2)
{
  int ret = starpu_task_submit (task (h1, h2));
  STARPU_CHECK_RETURN_VALUE (ret, "starpu_task_submit");
}


starpu_task* MyTask::task (const AthStarPU::Handle<CaloCellAccessor>& h1,
                           const starpu_data_handle_t& h2)
{
  starpu_task* task = starpu_task_create();
  task->cl = &m_cl;
  task->handles[0] = h1;
  task->handles[1] = h2;
  return task;
}


class MyTask2
{
public:
  MyTask2();
  void submit (const AthStarPU::Handle<CaloCellAccessor>& h1,
                     AthStarPU::Handle<CaloCellAccessor>& h2);
  starpu_task* task (const AthStarPU::Handle<CaloCellAccessor>& h1,
                           AthStarPU::Handle<CaloCellAccessor>& h2);


private:
  starpu_codelet m_cl;
};


MyTask2::MyTask2()
{
  starpu_codelet_init (&m_cl);
  m_cl.cpu_funcs[0] = cpu_func2;
  m_cl.cuda_funcs[0] = xaodtest_cuda_copy;
  m_cl.nbuffers = 2;
  m_cl.modes[0] = STARPU_R;
  // FIXME: W alone won't work because Accessor doesn't get copied.
  m_cl.modes[1] = STARPU_RW;
}


void MyTask2::submit (const AthStarPU::Handle<CaloCellAccessor>& h1,
                      AthStarPU::Handle<CaloCellAccessor>& h2)
{
  int ret = starpu_task_submit (task (h1, h2));
  STARPU_CHECK_RETURN_VALUE (ret, "starpu_task_submit");
}


starpu_task* MyTask2::task (const AthStarPU::Handle<CaloCellAccessor>& h1,
                                  AthStarPU::Handle<CaloCellAccessor>& h2)
{
  starpu_task* task = starpu_task_create();
  task->cl = &m_cl;
  task->handles[0] = h1;
  task->handles[1] = h2;
  return task;
}


int main (int /*argc*/, char** /*argv*/)
{
  AthStarPU::Init init;

  xAOD::CaloCellContainer ccc;
  xAOD::CaloCellAuxContainer auxstore;
  ccc.setStore (&auxstore);

  xAOD::CaloCellContainer ccc3;
  xAOD::CaloCellAuxContainer auxstore3;
  ccc3.setStore (&auxstore3);

  for (int i=0; i < 4; i++) {
    ccc.push_back (std::make_unique<xAOD::CaloCell>());
    ccc.back()->setHash (i);
    ccc.back()->setE (i+10);
    ccc.back()->setEta (i+20);
    ccc.back()->setPhi (i+20);

    ccc3.push_back (std::make_unique<xAOD::CaloCell>());
  }

  float v[4] = {0};

  {
    CaloCellAccessor acc (ccc);
    AthStarPU::Handle<CaloCellAccessor> handle1 (acc);

    CaloCellAccessor acc3 (ccc3);
    AthStarPU::Handle<CaloCellAccessor> handle3 (acc3);

    starpu_data_handle_t handle2;
    starpu_vector_data_register (&handle2, STARPU_MAIN_RAM,
                                 reinterpret_cast<uintptr_t>(v),
                                 4,
                                 sizeof(v[0]));

    MyTask mytask;
    mytask.submit (handle1, handle2);

    MyTask2 mytask2;
    mytask2.submit (handle1, handle3);

    starpu_task_wait_for_all();

    starpu_data_unregister (handle2);
  }

  for (int i=0; i < 4; i++) {
  std::cout << v[i] << " ";
  }
  std::cout << "\n";

  for (const xAOD::CaloCell* c : ccc3) {
    std::cout << c->e() << " " << c->eta() << " "
              << c->phi() << " " << c->hash() << "\n";
  }

  return 0;
}


// Accessor: general variables
// starpu dependence in DataVectorAccessor
// pinning of m_acc?
// ... tower building?
// ... const checking??



// Local Variables:
// compile-command: "bash -c 'cd /usatlas/u/snyder/gpu/demo; . ../setup; scons-3 -s -j4'"
// End:
