//  -*- C -*-.

#include <starpu.h>


static __global__ void vector_mult_cuda (unsigned int n,
                                         float* val,
                                         float factor)
{
  unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
  if (i < n)
    val[i] *= factor;
}


void scal_cuda_func (void* buffers[], void* args)
{
  float factor = *(float*)args;
  unsigned n = STARPU_VECTOR_GET_NX (buffers[0]);
  float* val = (float*) (STARPU_VECTOR_GET_PTR (buffers[0]));

  const unsigned int threads_per_block = 64;
  const unsigned int nblocks = (n + threads_per_block-1) / threads_per_block;

  vector_mult_cuda<<<nblocks, threads_per_block, 0,
                     starpu_cuda_get_local_stream()>>>
    (n, val, factor);

  cudaStreamSynchronize (starpu_cuda_get_local_stream());
}

