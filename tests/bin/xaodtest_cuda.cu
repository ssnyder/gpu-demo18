//  -*- C -*-.

#include "tests/DataVectorAccessor.h"
#include "tests/CaloCellAccessor.h"
#include "tests/DataVectorStarPUInterface.h"
#include <starpu.h>


#if 0
static __global__ void xaodtest_cuda1 (unsigned int n,
                                       float* v1,
                                       const CaloCellAccessor& acc)
{
  const float* arr1 = acc.eArray();
  const float* arr2 = acc.phiArray();
  unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
  if (i < n)
    v1[i] = arr1[i] + 2*arr2[i];
}
#endif


static __global__ void xaodtest_cuda2 (unsigned int n,
                                       float* v1,
                                       const CaloCellAccessor& acc)
{
  unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
  if (i < n)
    v1[i] = acc.e(i) + 2*acc.phi(i);
}


#if 0
static __global__ void xaodtest_cuda3 (unsigned int n,
                                       float* v1,
                                       const float* arr1,
                                       const float* arr2)
{
  unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
  if (i < n)
    v1[i] = arr1[i] + 2*arr2[i];
}
#endif


void xaodtest_cuda_func (void* buffers[], void* /*args*/)
{
  const CaloCellAccessor& acc = dv_accessor<CaloCellAccessor> (buffers[0]);
  unsigned int nv = STARPU_VECTOR_GET_NX (buffers[1]);
  STARPU_ASSERT_MSG (nv == acc.size(), "Size mismatch.");
  float* val = reinterpret_cast<float*> (STARPU_VECTOR_GET_PTR (buffers[1]));

  const unsigned int threads_per_block = 64;
  const unsigned int nblocks = (nv + threads_per_block-1) / threads_per_block;

#if 1
  const CaloCellAccessor& devacc = dv_dev_accessor<CaloCellAccessor> (buffers[0]);
  xaodtest_cuda2<<<nblocks, threads_per_block, 0,
                  starpu_cuda_get_local_stream()>>>
    (nv, val, devacc);
#endif

#if 0
  const CaloCellAccessor& hostacc = dv_host_accessor<CaloCellAccessor> (buffers[0]);
  const float* arr1 = hostacc.eArray();
  const float* arr2 = hostacc.phiArray();
  xaodtest_cuda3<<<nblocks, threads_per_block, 0,
                  starpu_cuda_get_local_stream()>>>
    (nv, val, arr1, arr2);
#endif

  cudaStreamSynchronize (starpu_cuda_get_local_stream());
}


static __global__ void xaodtest_cuda_copy (unsigned int n,
                                           const CaloCellAccessor& acc1,
                                                  CaloCellAccessor& acc2)
{
  unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
  if (i < n) {
    acc2.e(i) = acc1.e(i) * 1.5;
    acc2.eta(i) = acc1.eta(i);
    acc2.phi(i) = acc1.phi(i);
    acc2.hash(i) = acc1.hash(i);
  }
}


void xaodtest_cuda_copy (void* buffers[], void* /*args*/)
{
  const CaloCellAccessor& acc1 = dv_accessor<CaloCellAccessor> (buffers[0]);
        CaloCellAccessor& acc2 = dv_accessor<CaloCellAccessor> (buffers[0]);
  const size_t sz = acc1.size();
  STARPU_ASSERT_MSG (sz == acc2.size(), "Size mismatch.");

  const unsigned int threads_per_block = 64;
  const unsigned int nblocks = (sz + threads_per_block-1) / threads_per_block;

  const CaloCellAccessor& devacc1 = dv_dev_accessor<CaloCellAccessor> (buffers[0]);
        CaloCellAccessor& devacc2 = dv_dev_accessor<CaloCellAccessor> (buffers[1]);
  xaodtest_cuda_copy<<<nblocks, threads_per_block, 0,
                       starpu_cuda_get_local_stream()>>>
    (sz, devacc1, devacc2);

  cudaStreamSynchronize (starpu_cuda_get_local_stream());
}

