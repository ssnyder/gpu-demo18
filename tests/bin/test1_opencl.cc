#include <starpu.h>
#include <iostream>


namespace {


class ProgramHolder
{
public:
  ~ProgramHolder();
  void load (const char* fname);
  void load_kernel (const char* kname,
                    cl_kernel& kernel,
                    cl_command_queue& queue);

private:
  starpu_opencl_program m_programs;
};


void ProgramHolder::load (const char* fname)
{
  int err = starpu_opencl_load_opencl_from_file (fname, &m_programs, NULL);
  if (err != CL_SUCCESS) STARPU_OPENCL_REPORT_ERROR (err);
  std::cout << "loaded!\n";
}


void ProgramHolder::load_kernel (const char* kname,
                                 cl_kernel& kernel,
                                 cl_command_queue& queue)
{
  const int id = starpu_worker_get_id();
  const int devid = starpu_worker_get_devid (id);
  int err = starpu_opencl_load_kernel (&kernel,
                                       &queue,
                                       &m_programs,
                                       kname,
                                       devid);
  if (err != CL_SUCCESS) STARPU_OPENCL_REPORT_ERROR (err);
}


ProgramHolder::~ProgramHolder()
{
  starpu_opencl_unload_opencl (&m_programs);
}


ProgramHolder programs;


} // anonymous namespace


void scal_opencl_init()
{
  programs.load ("tests/bin/vector_mult_opencl.cl");
}


void scal_opencl_func (void* buffers[], void* args)
{
  float factor = *reinterpret_cast<float*> (args);

  const unsigned int n = STARPU_VECTOR_GET_NX (buffers[0]);
  cl_mem val = reinterpret_cast<cl_mem> (STARPU_VECTOR_GET_DEV_HANDLE (buffers[0]));

  const int id = starpu_worker_get_id();
  const int devid = starpu_worker_get_devid (id);

  cl_kernel kernel;
  cl_command_queue queue;
  {
    programs.load_kernel ("vector_mult_opencl", kernel, queue);
    int err = clSetKernelArg (kernel, 0, sizeof(n), &n);
    err |= clSetKernelArg (kernel, 1, sizeof(val), &val);
    err |= clSetKernelArg (kernel, 2, sizeof(factor), &factor);
    if (err != CL_SUCCESS) STARPU_OPENCL_REPORT_ERROR (err);
  }

  cl_event event;
  {
    cl_device_id device;
    starpu_opencl_get_device (devid, &device);
    size_t local, s;
    int err = clGetKernelWorkGroupInfo (kernel,
                                        device,
                                        CL_KERNEL_WORK_GROUP_SIZE,
                                        sizeof(local), &local, &s);
    if (err != CL_SUCCESS) STARPU_OPENCL_REPORT_ERROR (err);

    const size_t global = n;
    if (local > global) local = global;

    err = clEnqueueNDRangeKernel (queue, kernel, 1, NULL,
                                  &global, &local, 0,
                                  NULL, &event);
    if (err != CL_SUCCESS) STARPU_OPENCL_REPORT_ERROR (err);
  }

  clFinish (queue);
  starpu_opencl_collect_stats (event);
  clReleaseEvent (event);
  starpu_opencl_release_kernel (kernel);
}
