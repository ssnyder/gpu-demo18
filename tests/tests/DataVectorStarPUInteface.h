// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration.
 */
// $Id$
/**
 * @file /DataVectorStarPUInteface.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2018
 * @brief 
 */


#ifndef TESTS_DATAVECTORSTARPUINTEFACE_H
#define TESTS_DATAVECTORSTARPUINTEFACE_H


#include "tests/DataVectorAccessor.h"
#include <starpu.h>


struct DataVectorStarPUInterface
{
public:
  DataVectorStarPUInterface (DataVectorAccessor* dv);
  DataVectorStarPUInterface (const DataVectorAccessor* dv);
  DataVectorStarPUInterface (const DataVectorStarPUInterface& intf,
                             bool isHomeNode);
  starpu_ssize_t allocOnNode (unsigned int node);
  void freeOnNode (unsigned int node);
  size_t nelt() const { return m_nelt; }
  size_t size() const; // Total allocation size in bytes
  uint32_t footprint() const;
  int copy (unsigned int dst_node,
            const DataVectorStarPUInterface& src_intf,
            unsigned int src_node,
            void* async_data);

  void checkID() const {} // xxx

  DataVectorAccessor& acc() { return *m_ptr; }


private:
  DataVectorAccessor* m_ptr;
  size_t m_nvar;
  size_t m_nelt;
  bool m_isConst;
};


#endif // not TESTS_DATAVECTORSTARPUINTEFACE_H
