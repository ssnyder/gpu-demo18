// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration.
 */
// $Id$
/**
 * @file /DataVectorStarPUInterface.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2018
 * @brief 
 */


#ifndef TESTS_DATAVECTORSTARPUINTERFACE_H
#define TESTS_DATAVECTORSTARPUINTERFACE_H


#include "tests/DataVectorAccessor.h"
#include <starpu.h>


struct DataVectorStarPUInterface
{
public:
  DataVectorStarPUInterface (DataVectorAccessor* dv,
                             size_t accSize,
                             starpu_data_interface_id id);
  DataVectorStarPUInterface (const DataVectorAccessor* dv,
                             size_t accSize,
                             starpu_data_interface_id id);
  starpu_ssize_t allocOnNode (unsigned int node);
  void freeOnNode (unsigned int node);
  size_t nelt() const { return m_acc.size(); }
  size_t size() const; // Total allocation size in bytes
  uint32_t footprint() const;
  int copy (unsigned int dst_node,
            const DataVectorStarPUInterface& src_intf,
            unsigned int src_node,
            void* async_data);

  void checkID (starpu_data_interface_id id) const;

  DataVectorAccessor& acc() { return m_acc; }
  const DataVectorAccessor& acc() const { return m_acc; }
  DataVectorAccessor& devacc() { return *m_devacc; }
  const DataVectorAccessor& devacc() const { return *m_devacc; }
  DataVectorAccessor& hostacc() { return *m_hostacc; }
  const DataVectorAccessor& hostacc() const { return *m_hostacc; }

  void setPointersInCopy (bool isHomeNode);


private:
  starpu_data_interface_id m_id;
  size_t m_accSize;
  // Host-side copy, with m_cache pointing at host memory.
  DataVectorAccessor* m_hostacc;
  // Device-side copy.
  DataVectorAccessor* m_devacc;
  void* m_cacheBuf;
  bool m_isConst; // FIXME: unused.
  // Must be at end.  We overallocate the space for this class and follow
  // this with the extra data at the end of the derived Accessor class.
  DataVectorAccessor m_acc;
};


inline
void DataVectorStarPUInterface::checkID (starpu_data_interface_id id) const
{
  STARPU_ASSERT_MSG (m_id == id, "ID mismatch.");
}


//******************************************************************


struct DataVectorStarPUOps
{
public:
  typedef void (*register_handle_fn) (starpu_data_handle_t handle,
                                      unsigned int home_node,
                                      void* data_interface);

  DataVectorStarPUOps (starpu_data_interface_id interfaceid,
                       register_handle_fn register_handle,
                       size_t accSize,
                       const char* name);
  starpu_data_interface_id interfaceid() const { return m_ops.interfaceid; }

  // Really should be const ... but underlying starpu interfaces aren't.
  starpu_data_interface_ops* ops() { return &m_ops; }
  starpu_data_interface_id id() const { return m_ops.interfaceid; }


protected:
  static void do_register (starpu_data_handle_t handle,
                           unsigned int home_node,
                           void* data_interface,
                           size_t accSize);
  

private:
  static DataVectorStarPUInterface& cast (void* data_interface)
  { return *reinterpret_cast<DataVectorStarPUInterface*>(data_interface); }
  static DataVectorStarPUInterface& fromNode (starpu_data_handle_t handle,
                                              unsigned int node)
  { return cast (starpu_data_get_interface_on_node (handle, node)); }

  static starpu_ssize_t allocate_data_on_node (void* data_interface,
                                               unsigned int node);
  static void free_data_on_node (void *data_interface,
                                 unsigned int node);
  static starpu_ssize_t describe (void* data_interface,
                                  char* buf,
                                  size_t size);
  static size_t get_size (starpu_data_handle_t handle);
  static uint32_t footprint (starpu_data_handle_t handle);
  static int copy_any_to_any (void *src_interface, unsigned src_node,
                              void *dst_interface, unsigned dst_node,
                              void *async_data);
                                    
  starpu_data_interface_ops m_ops;
  starpu_data_copy_methods m_copy;
};


//******************************************************************


template <class ACC>
class SpecificDVStarPUOps
  : public DataVectorStarPUOps
{
public:
  SpecificDVStarPUOps();


private:
  static void register_data_handle (starpu_data_handle_t handle,
                                    unsigned int home_node,
                                    void* data_interface);
};


template <class ACC>
SpecificDVStarPUOps<ACC>::SpecificDVStarPUOps()
  : DataVectorStarPUOps (static_cast<starpu_data_interface_id>(starpu_data_interface_get_next_id()),
                         register_data_handle,
                         sizeof (ACC),
                         "CaloCellAccessorXXX")
{
}


template <class ACC>
void SpecificDVStarPUOps<ACC>::register_data_handle (starpu_data_handle_t handle,
                                                     unsigned int home_node,
                                                     void* data_interface)
{
  do_register (handle, home_node, data_interface, sizeof (ACC));
}




template <class ACC>
class SpecificDVStarPUInterface
  : public DataVectorStarPUInterface
{
public:
  SpecificDVStarPUInterface (ACC* dv)
    : DataVectorStarPUInterface (dv, sizeof(ACC), m_ops.id())
  {}
  static starpu_data_interface_id id() { return m_ops.id(); }
  // Really should be const ... but underlying starpu interfaces aren't.
  static starpu_data_interface_ops* ops() { return m_ops.ops(); }


  // Not private to avoid clang warning.
  char m_pad[sizeof(ACC) - sizeof(DataVectorAccessor)];
private:
  static SpecificDVStarPUOps<ACC> m_ops;
};


template <class ACC>
SpecificDVStarPUOps<ACC> SpecificDVStarPUInterface<ACC>::SpecificDVStarPUInterface::m_ops;


//******************************************************************


template <class ACC>
inline
ACC& dv_accessor (void* buf)
{
  DataVectorStarPUInterface& intf = *reinterpret_cast<DataVectorStarPUInterface*> (buf);
  intf.checkID (SpecificDVStarPUInterface<ACC>::id());
  intf.acc().checkCache();
  return reinterpret_cast<ACC&>(intf.acc());
}


template <class ACC>
inline
ACC& dv_dev_accessor (void* buf)
{
  DataVectorStarPUInterface& intf = *reinterpret_cast<DataVectorStarPUInterface*> (buf);
  intf.checkID (SpecificDVStarPUInterface<ACC>::id());
  intf.acc().checkCache();
  return reinterpret_cast<ACC&>(intf.devacc());
}


template <class ACC>
inline
ACC& dv_host_accessor (void* buf)
{
  DataVectorStarPUInterface& intf = *reinterpret_cast<DataVectorStarPUInterface*> (buf);
  intf.checkID (SpecificDVStarPUInterface<ACC>::id());
  intf.hostacc().checkCache();
  return reinterpret_cast<ACC&>(intf.hostacc());
}


template <class ACC>
void dv_data_register (starpu_data_handle_t* handle,
                       unsigned home_node,
                       ACC* dv)
{
  SpecificDVStarPUInterface<ACC> intf (dv);
  starpu_data_register (handle, home_node, &intf,
                        SpecificDVStarPUInterface<ACC>::ops());
}


//******************************************************************


namespace AthStarPU {


template <class ACC>
class Handle
{
public:
  Handle (ACC& acc)
  {
    dv_data_register (&m_handle, STARPU_MAIN_RAM, &acc);
  }

  ~Handle()
  {
    starpu_data_unregister (m_handle);
  }


  operator starpu_data_handle_t() const 
  {
    return m_handle;
  }


private:
  starpu_data_handle_t m_handle;
};


} // namespace AthStarPU

#endif // not TESTS_DATAVECTORSTARPUINTERFACE_H
