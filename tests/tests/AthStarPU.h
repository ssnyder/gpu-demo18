// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration.
 */
// $Id$
/**
 * @file /AthStarPU.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2018
 * @brief 
 */


#ifndef TESTS_ATHSTARPU_H
#define TESTS_ATHSTARPU_H


#include <starpu.h>


#if defined(__CUDACC__)
# define ATH_DEVHOST  __device__ __host__
#else
# define ATH_DEVHOST  __device__ __host__
#endif


namespace AthStarPU {

class Init
{
public:
  Init();
  ~Init();
};


} // namespace AthStarPU


#endif // not TESTS_ATHSTARPU_H
