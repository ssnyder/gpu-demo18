// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration.
 */
// $Id$
/**
 * @file /CaloCellAccessor.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2018
 * @brief 
 */


#ifndef TESTS_CALOCELLACCESSOR_H
#define TESTS_CALOCELLACCESSOR_H


#include "tests/DataVectorAccessor.h"


class CaloCellAccessor
  : public DataVectorAccessor
{
public:
#if !defined(__CUDACC__)
  CaloCellAccessor (const xAOD::CaloCellContainer& ccc);
#endif


  ATH_DEVHOST
  const float* eArray() const
  {
    return arr<float>(m_eId);
  }

  ATH_DEVHOST
  float* eArray()
  {
    return arr<float>(m_eId);
  }

  ATH_DEVHOST
  float e (size_t ndx) const
  {
    return arr<float>(m_eId)[ndx];
  }

  ATH_DEVHOST
  float& e (size_t ndx)
  {
    return arr<float>(m_eId)[ndx];
  }


  ATH_DEVHOST
  const float* etaArray() const
  {
    return arr<float>(m_etaId);
  }

  ATH_DEVHOST
  float* etaArray()
  {
    return arr<float>(m_etaId);
  }

  ATH_DEVHOST
  float eta (size_t ndx) const
  {
    return arr<float>(m_etaId)[ndx];
  }

  ATH_DEVHOST
  float& eta (size_t ndx)
  {
    return arr<float>(m_etaId)[ndx];
  }


  ATH_DEVHOST
  const float* phiArray() const
  {
    return arr<float>(m_phiId);
  }

  ATH_DEVHOST
  float* phiArray()
  {
    return arr<float>(m_phiId);
  }

  ATH_DEVHOST
  float phi (size_t ndx) const
  {
    return arr<float>(m_phiId)[ndx];
  }

  ATH_DEVHOST
  float& phi (size_t ndx)
  {
    return arr<float>(m_phiId)[ndx];
  }


  ATH_DEVHOST
  const int* hashArray() const
  {
    return arr<int>(m_hashId);
  }

  ATH_DEVHOST
  int* hashArray()
  {
    return arr<int>(m_hashId);
  }

  ATH_DEVHOST
  int hash (size_t ndx) const
  {
    return arr<int>(m_hashId)[ndx];
  }

  ATH_DEVHOST
  int& hash (size_t ndx)
  {
    return arr<int>(m_hashId)[ndx];
  }


private:
  SG::auxid_t m_eId;
  SG::auxid_t m_etaId;
  SG::auxid_t m_phiId;
  SG::auxid_t m_hashId;
};


#endif // not TESTS_CALOCELLACCESSOR_H
