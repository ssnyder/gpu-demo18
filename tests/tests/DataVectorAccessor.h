// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration.
 */
// $Id$
/**
 * @file /DataVectorAccessor.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2018
 * @brief 
 */


#ifndef TESTS_DATAVECTORACCESSOR_H
#define TESTS_DATAVECTORACCESSOR_H


#include "AthContainersInterfaces/AuxTypes.h"
#if !defined(__CUDACC__)
# include "xAODCaloEvent/CaloCellContainer.h"
#endif
#include "tests/AthStarPU.h"


class DataVectorStarPUInterface;

class DataVectorAccessor
{
public:
#if !defined(__CUDACC__)
  DataVectorAccessor (const SG::AuxVectorData& ccc,
                      size_t sz,
                      size_t accSize);
#endif
  DataVectorAccessor (const DataVectorAccessor& other,
                      bool copyPointers);
  ~DataVectorAccessor();
  long alloc (unsigned int node);
  void free (unsigned int node);
  size_t size() const { return m_size; }
  size_t nvar() const { return m_nvar; }
  size_t datasize() const { return m_datasize; }
  void checkCache() const;
  size_t tailSize() const { return m_tailSize; }


protected:
  template <class T>
  ATH_DEVHOST
  const T* arr (SG::auxid_t id) const { return (const T*)m_cache[id]; }
  template <class T>
  ATH_DEVHOST
  T* arr (SG::auxid_t id) { return (T*)m_cache[id]; }


private:
  // For copy().
  friend class DataVectorStarPUInterface;

  void setPointersInCopy (bool isHomeNode);


  struct VarDesc
  {
    VarDesc (SG::auxid_t id, size_t eltsize, size_t offs)
      : m_id (id), m_eltsize (eltsize), m_offs (offs)
    {}
    SG::auxid_t m_id;
    size_t m_eltsize;
    size_t m_offs;
  };

  void fillCache (void** cache, const VarDesc* vars);

  size_t m_size;
  size_t m_nvar;
  size_t m_datasize;
  size_t m_cachelen;
  size_t m_tailSize;

  void* m_data;
  VarDesc* m_vars;
  void** m_cache;

  bool m_owndata;
  bool m_contig;
  bool m_cacheValid;
};


inline
long DataVectorAccessor::alloc (unsigned int node)
{
  if (m_size > 0 && m_nvar > 0) {
    m_contig = true;
    m_cacheValid = false;

    m_data = reinterpret_cast<void*> (starpu_malloc_on_node (node, m_datasize));
    if (!m_data) return -ENOMEM;
    m_vars = reinterpret_cast<VarDesc*> (m_data);
    const size_t var_desc_size = m_nvar * sizeof (VarDesc);
    m_cache = reinterpret_cast<void**>(reinterpret_cast<char*>(m_data) + var_desc_size);

    return m_datasize;
  }
  return 0;
}


inline
void DataVectorAccessor::free (unsigned int node)
{
  if (m_data) {
    starpu_free_on_node (node,
                         reinterpret_cast<uintptr_t> (m_data),
                         m_datasize);
    m_data = 0;
    m_vars = 0;
    m_cache = 0;
    m_cacheValid = false;
  }
}


inline
void DataVectorAccessor::fillCache (void** cache, const VarDesc* vars)
{
  STARPU_ASSERT_MSG (m_contig, "fillCache");
  const size_t cachesize = m_cachelen * sizeof(void*);
  uintptr_t bufs = reinterpret_cast<uintptr_t>(m_cache) + cachesize;

  std::fill (cache, cache+m_cachelen, static_cast<void*>(0));
  for (size_t iv = 0; iv < m_nvar; iv++) {
    const VarDesc& d = vars[iv];
    cache[d.m_id] = reinterpret_cast<void*> (bufs + d.m_offs);
  }
}


inline
void DataVectorAccessor::checkCache() const
{
  STARPU_ASSERT_MSG (m_cacheValid, "checkCache");
}


#endif // not TESTS_DATAVECTORACCESSOR_H
