// $Id$
/**
 * @file DataVectorAccessor.cc
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2018
 * @brief 
 */


#include "tests/DataVectorAccessor.h"


DataVectorAccessor::DataVectorAccessor (const SG::AuxVectorData& ccc,
                                        size_t sz,
                                        size_t accSize)
  : m_size (sz),
    m_nvar (ccc.getAuxIDs().size()),
    m_tailSize (accSize - sizeof(DataVectorAccessor))
{
  const SG::AuxTypeRegistry& r = SG::AuxTypeRegistry::instance();

  m_cachelen = 0;
  const size_t var_desc_size = m_nvar * sizeof (VarDesc);

  size_t offs = 0;
  std::vector<VarDesc> descs;
  descs.reserve (m_nvar);
  for (SG::auxid_t id : ccc.getAuxIDs()) {
    m_cachelen = std::max (m_cachelen, id+1);
    size_t eltsize = r.getEltSize (id);
    descs.emplace_back (id, eltsize, offs);
    offs += m_size * eltsize;
  }

  m_datasize = var_desc_size;
  m_datasize += m_cachelen * sizeof(void*);

  starpu_malloc (&m_data, m_datasize);
  m_vars = reinterpret_cast<VarDesc*> (m_data);
  std::copy (descs.begin(), descs.end(), m_vars);

  m_cache = reinterpret_cast<void**>(reinterpret_cast<char*>(m_data) + var_desc_size);
  std::fill (m_cache, m_cache+m_cachelen, static_cast<void*>(0));
  for (const VarDesc& d : descs) {
    // FIXME: const_cast.
    void* buf = const_cast<void*> (ccc.getDataArray (d.m_id));
    m_cache[d.m_id] = buf;
    // FIXME: return failure if no cuda device.
    /*int ret =*/ starpu_memory_pin (buf, d.m_eltsize * m_size);
    //STARPU_ASSERT_MSG (ret == 0, "memory_pin");
  }

  m_datasize += offs;

  m_owndata = true;
  m_contig = false;
  m_cacheValid = true;
}


DataVectorAccessor::DataVectorAccessor (const DataVectorAccessor& other,
                                        bool copyPointers)
  : m_size (other.m_size),
    m_nvar (other.m_nvar),
    m_datasize (other.m_datasize),
    m_cachelen (other.m_cachelen),
    m_tailSize (other.m_tailSize),
    m_owndata (false)
{
  if (copyPointers) {
    m_data = other.m_data;
    m_cache = other.m_cache;
    m_vars = other.m_vars;
    m_contig = other.m_contig;
    m_cacheValid = other.m_cacheValid;
  }
  else {
    m_data = 0;
    m_cache = 0;
    m_vars = 0;
    m_contig = false;
    m_cacheValid = false;
  }
}


void DataVectorAccessor::setPointersInCopy (bool isHomeNode)
{
  m_owndata = false;
  if (!isHomeNode) {
    m_data = 0;
    m_cache = 0;
    m_vars = 0;
    m_contig = false;
    m_cacheValid = false;
  }
}


DataVectorAccessor::~DataVectorAccessor()
{
  if (m_owndata) {
    for (size_t iv = 0; iv < m_nvar; iv++) {
      const VarDesc& d  = m_vars[iv];
      /*int ret =*/ starpu_memory_unpin (m_cache[d.m_id], d.m_eltsize * m_size);
      //STARPU_ASSERT_MSG (ret == 0, "memory_unpin");
    }

    starpu_free (m_data);
  }
}


