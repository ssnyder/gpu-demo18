// $Id$
/**
 * @file AthStarPU.cc
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2018
 * @brief 
 */


#include "tests/AthStarPU.h"


AthStarPU::Init::Init()
{
  int ret = starpu_init (NULL);
  STARPU_CHECK_RETURN_VALUE (ret, "starpu_init");
}


AthStarPU::Init::~Init()
{
  starpu_shutdown();
}

