// $Id$
/**
 * @file DataVectorStarPUInterface.cc
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2018
 * @brief 
 */

#include "tests/DataVectorStarPUInterface.h"


starpu_ssize_t DataVectorStarPUInterface::allocOnNode (unsigned int node)
{
  starpu_ssize_t sz = m_acc.alloc (node);
  if (node != STARPU_MAIN_RAM) {
    m_devacc = reinterpret_cast<DataVectorAccessor*> (starpu_malloc_on_node (node, m_accSize));
    if (!m_devacc) {
      m_acc.free (node);
      return -ENOMEM;
    }
    sz += m_accSize;

    starpu_malloc (reinterpret_cast<void**>(&m_hostacc), m_accSize);
  }
  return sz;
}


void DataVectorStarPUInterface::freeOnNode (unsigned int node)
{
  m_acc.free (node);
  if (m_cacheBuf) {
    starpu_free (m_cacheBuf);
    m_cacheBuf = 0;
  }
  if (node != STARPU_MAIN_RAM) {
    starpu_free_on_node (node, reinterpret_cast<uintptr_t> (m_devacc), m_accSize);
    m_devacc = &m_acc;

    starpu_free (m_hostacc);
    m_hostacc = &m_acc;
  }
}


size_t DataVectorStarPUInterface::size() const
{
  size_t sz = m_acc.size()*sizeof(float) + m_acc.datasize() + m_accSize;
  if (m_devacc != &m_acc) {
    sz += m_accSize;
  }
  return sz;
}


uint32_t DataVectorStarPUInterface::footprint() const
{
  uint32_t hash = starpu_hash_crc32c_be (m_acc.size(), 0);
  hash = starpu_hash_crc32c_be (m_acc.nvar(), hash);
  hash = starpu_hash_crc32c_be (m_acc.datasize(), hash);
  hash = starpu_hash_crc32c_be_n (m_acc.m_vars, m_acc.nvar()*sizeof(DataVectorAccessor::VarDesc), hash);
  return hash;
}


int DataVectorStarPUInterface::copy (unsigned int dst_node,
                                     const DataVectorStarPUInterface& src_intf,
                                     unsigned int src_node,
                                     void* async_data)
{
  if (m_cacheBuf) {
    starpu_free (m_cacheBuf);
    m_cacheBuf = 0;
  }

  size_t nvar = m_acc.nvar();

  const DataVectorAccessor& src_acc = src_intf.acc();
  DataVectorAccessor& dst_acc = acc();

  int ret = 0;

  const size_t var_desc_size = nvar * sizeof (DataVectorAccessor::VarDesc);
  if (starpu_interface_copy (reinterpret_cast<uintptr_t> (src_acc.m_vars),
                             0, src_node,
                             reinterpret_cast<uintptr_t> (dst_acc.m_vars),
                             0, dst_node,
                             var_desc_size,
                             async_data))
  {
    ret = -EAGAIN;
  }

  const size_t cachesize = src_acc.m_cachelen * sizeof(void*);
  uintptr_t src_bufs = reinterpret_cast<uintptr_t>(src_acc.m_cache) + cachesize;
  uintptr_t dst_bufs = reinterpret_cast<uintptr_t>(dst_acc.m_cache) + cachesize;

  if (src_acc.m_contig && dst_acc.m_contig) {
    if (starpu_interface_copy (src_bufs, 0, src_node,
                               dst_bufs, 0, dst_node,
                               src_acc.m_datasize - var_desc_size - cachesize,
                               async_data))
    {
      ret = -EAGAIN;
    }
  }

  else if (!src_acc.m_contig && dst_acc.m_contig) {
    STARPU_ASSERT_MSG (STARPU_MAIN_RAM == src_node, "Bad copy");
    const DataVectorAccessor::VarDesc* vars = src_acc.m_vars;
    for (size_t iv = 0; iv < nvar; iv++) {
      const DataVectorAccessor::VarDesc& d = vars[iv];
      if (starpu_interface_copy (reinterpret_cast<uintptr_t> (src_acc.m_cache[d.m_id]),
                                 0, src_node,
                                 dst_bufs + d.m_offs, 0, dst_node,
                                 d.m_eltsize * m_acc.m_size,
                                 async_data))
      {
        ret = -EAGAIN;
      }
    }
  }
  else if (src_acc.m_contig && !dst_acc.m_contig) {
    STARPU_ASSERT_MSG (STARPU_MAIN_RAM == dst_node, "Bad copy");
    const DataVectorAccessor::VarDesc* vars = dst_acc.m_vars;
    for (size_t iv = 0; iv < nvar; iv++) {
      const DataVectorAccessor::VarDesc& d = vars[iv];
      if (starpu_interface_copy (src_bufs + vars[iv].m_offs, 0, src_node,
                                 reinterpret_cast<uintptr_t> (dst_acc.m_cache[d.m_id]),
                                 0, dst_node,
                                 d.m_eltsize * m_acc.m_size,
                                 async_data))
      {
        ret = -EAGAIN;
      }
    }
  }
  else {
    STARPU_ABORT_MSG ("Bad copy");
  }

  if (!dst_acc.m_cacheValid) {
    STARPU_ASSERT_MSG (dst_acc.m_contig, "Bad copy");
    if (dst_node == STARPU_MAIN_RAM) {
      dst_acc.fillCache (dst_acc.m_cache, dst_acc.m_vars);
    }
    else {
      starpu_malloc (&m_cacheBuf, cachesize);
      // FIXME: Find the MAIN_RAM interface if needed.
      STARPU_ASSERT_MSG (src_node == STARPU_MAIN_RAM, "Bad copy");
      dst_acc.fillCache (reinterpret_cast<void**>(m_cacheBuf), src_acc.m_vars);
      if (starpu_interface_copy (reinterpret_cast<uintptr_t> (m_cacheBuf),
                                 0, STARPU_MAIN_RAM,
                                 reinterpret_cast<uintptr_t> (dst_acc.m_cache),
                                 0, dst_node,
                                 cachesize,
                                 async_data))
      {
        ret = -ENOMEM;
      }
    }
    dst_acc.m_cacheValid = true;
  }

  if (m_devacc != &m_acc) {
    // pin source?
    if (starpu_interface_copy (reinterpret_cast<uintptr_t> (&m_acc),
                               0, src_node,
                               reinterpret_cast<uintptr_t> (m_devacc),
                               0, dst_node,
                               m_accSize,
                               async_data))
    {
      ret = -EAGAIN;
    }
  }

  if (m_hostacc != &m_acc) {
    memcpy (m_hostacc, &m_acc, m_accSize);
    if (m_cacheBuf) {
      m_hostacc->m_cache = reinterpret_cast<void**>(m_cacheBuf);
    }
  }

  return ret;
}


DataVectorStarPUInterface::DataVectorStarPUInterface (DataVectorAccessor* dv,
                                                      size_t accSize,
                                                      starpu_data_interface_id id)
  : m_id (id),
    m_accSize (accSize),
    m_hostacc (&m_acc),
    m_devacc (&m_acc),
    m_cacheBuf (0),
    m_isConst (false),
    m_acc (*dv, true)
{
  memcpy (((char*)&m_acc) + sizeof(DataVectorAccessor),
          ((char*)dv) + sizeof(DataVectorAccessor),
          dv->tailSize());
}


                      
DataVectorStarPUInterface::DataVectorStarPUInterface (const DataVectorAccessor* dv,
                                                      size_t accSize,
                                                      starpu_data_interface_id id)
  : DataVectorStarPUInterface (const_cast<DataVectorAccessor*> (dv),
                               accSize,
                               id)
{
  m_isConst = true;
}


void DataVectorStarPUInterface::setPointersInCopy (bool isHomeNode)
{
  m_hostacc = &m_acc;
  m_devacc = &m_acc;
  m_cacheBuf = 0;
  m_acc.setPointersInCopy (isHomeNode);
}


//******************************************************************


DataVectorStarPUOps::DataVectorStarPUOps (starpu_data_interface_id interfaceid,
                                          register_handle_fn register_handle,
                                          size_t accSize,
                                          const char* name)
{
  m_ops.register_data_handle = register_handle;
  m_ops.interface_size = sizeof (DataVectorStarPUInterface) + accSize - sizeof(DataVectorAccessor);
  m_ops.name = const_cast<char*> (name);
  m_ops.allocate_data_on_node = allocate_data_on_node;
  m_ops.free_data_on_node = free_data_on_node;
  m_ops.copy_methods = &m_copy;
  m_ops.handle_to_pointer = 0;
  m_ops.get_size = get_size;
  m_ops.footprint = footprint;
  m_ops.compare = 0;
  m_ops.display = 0;
  m_ops.describe = describe;
  m_ops.interfaceid = interfaceid;
  m_ops.is_multiformat = false;
  m_ops.dontcache = false;
  m_ops.get_mf_ops = 0;
  m_ops.pack_data = 0;
  m_ops.unpack_data = 0;

  memset (&m_copy, 0, sizeof(m_copy));
  m_copy.any_to_any = copy_any_to_any;
}


void DataVectorStarPUOps::do_register (starpu_data_handle_t handle,
                                       unsigned int home_node,
                                       void* data_interface,
                                       size_t accSize)
{
  DataVectorStarPUInterface& intf = cast (data_interface);
  for (unsigned int node = 0; node < STARPU_MAXNODES; node++) {
    DataVectorStarPUInterface& new_intf = fromNode (handle, node);
    memcpy (&new_intf, &intf, sizeof (DataVectorStarPUInterface) + accSize - sizeof(DataVectorAccessor));
    new_intf.setPointersInCopy (node == home_node);
  }
}


starpu_ssize_t DataVectorStarPUOps::allocate_data_on_node (void* data_interface,
                                                           unsigned int node)
{
  DataVectorStarPUInterface& intf = cast (data_interface);
  return intf.allocOnNode (node);
}


void DataVectorStarPUOps::free_data_on_node (void *data_interface,
                                             unsigned int node)
{
  DataVectorStarPUInterface& intf = cast (data_interface);
  return intf.freeOnNode (node);
}


starpu_ssize_t DataVectorStarPUOps::describe (void* data_interface,
                                              char* buf,
                                              size_t size)
{
  DataVectorStarPUInterface& intf = cast (data_interface);
  return snprintf (buf, size, "DataVector%zd", intf.nelt());
}


size_t DataVectorStarPUOps::get_size (starpu_data_handle_t handle)
{
  DataVectorStarPUInterface& intf = fromNode (handle, STARPU_MAIN_RAM);
  return intf.size();
}


uint32_t DataVectorStarPUOps::footprint (starpu_data_handle_t handle)
{
  DataVectorStarPUInterface& intf = fromNode (handle, STARPU_MAIN_RAM);
  return intf.footprint();
}


int DataVectorStarPUOps::copy_any_to_any (void *src_interface, unsigned src_node,
                                          void *dst_interface, unsigned dst_node,
                                          void *async_data)
{
  DataVectorStarPUInterface& src_intf = cast (src_interface);
  DataVectorStarPUInterface& dst_intf = cast (dst_interface);
  return dst_intf.copy (dst_node, src_intf, src_node, async_data);
}


