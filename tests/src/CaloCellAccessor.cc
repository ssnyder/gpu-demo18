// $Id$
/**
 * @file CaloCellAccessor.cc
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2018
 * @brief 
 */


#include "xAODCaloEvent/CaloCell.h"
#include "tests/CaloCellAccessor.h"


CaloCellAccessor::CaloCellAccessor (const xAOD::CaloCellContainer& ccc)
  : DataVectorAccessor (ccc, ccc.size(), sizeof(CaloCellAccessor))
{
  static const xAOD::CaloCell::Accessor<float> e_f ("e_f");
  m_eId = e_f.auxid();

  static const xAOD::CaloCell::Accessor<float> eta_f ("eta_f");
  m_etaId = eta_f.auxid();

  static const xAOD::CaloCell::Accessor<float> phi_f ("phi_f");
  m_phiId = phi_f.auxid();

  static const xAOD::CaloCell::Accessor<int> hash ("hash");
  m_hashId = hash.auxid();
}
